var gamePattern = []
var userClickedPattern = []
var buttonColours = ["red", "blue", "green", "yellow"]
var isGameStart = false
let currentLevel = 0
var isStartOver = false
//if user clicked A button game will be started
$(document).on('keypress',function(e) {
    console.log(e.which);
    if(e.which == 97 || e.which == 65) {
        if (isGameStart != true) {
            isGameStart = true
            nextSequence()
        }else if(isStartOver === true){
            isStartOver === false
            nextSequence()
        }
    }else{
        if ( isStartOver === true) {
            isStartOver === false
            nextSequence()
        }
    }
})


$(function() {
    $(".btn").click(function() {
        if (isGameStart == true) {
            //get value from attribute id 
            var userChosenColour  = $(this).attr("id")
            console.log(userChosenColour);
            //show animation
            animatePress(userChosenColour)
            //save the user activity
            userClickedPattern.push(userChosenColour)
            //check the answer

            var result = checkAnswer()
            console.log('result', result);
            if(result === 'true'){
                setTimeout(function() {
                    nextSequence()
                }, 1000);
                
            }else if(result == 'false'){
                $('#level-title').text('Game Over, Press Any Key to Restart')
                var audio = new Audio('sounds/wrong.mp3')
                audio.play();
                $("body").attr("class","game-over")
                setTimeout(function() {
                    $("body").removeClass("game-over")
                }, 200);
                startOver()
            }
        }
    })
})

function nextSequence(){
   var randomNumber =  Math.floor(Math.random() * 4);
   var randomChosenColour = buttonColours[randomNumber]
   //play the sound color
   playSound(randomChosenColour)
   gamePattern.push(randomChosenColour)
   //show animation
   $('#'+randomChosenColour).delay(100).fadeOut().fadeIn('slow')
   //show level game
   currentLevel = gamePattern.length;
   console.log(currentLevel, gamePattern, userClickedPattern);
   $('#level-title').text('Level '+currentLevel)
  
}

function playSound(userChosenColour) {
    var audio = new Audio('sounds/'+userChosenColour+'.mp3')
    audio.play();
}

function animatePress(currentColour) {
    $('#'+currentColour).addClass('pressed')
    playSound(currentColour)
    setTimeout(function() {
        $('#'+currentColour).removeClass('pressed')
    }, 200);
}

function checkAnswer() {
    console.log(userClickedPattern, 'length');
    for (let index = 0; index < userClickedPattern.length; index++) {
        // const element = array[index];  
        if(gamePattern[userClickedPattern.length - 1] === userClickedPattern[userClickedPattern.length - 1]){
            console.log(gamePattern[index] === userClickedPattern[index], gamePattern[index], userClickedPattern[index]);
            if (userClickedPattern.length === gamePattern.length) {
                userClickedPattern = [];
                 return 'true';      
            }
            return 'onprogress';
        }else{
            return 'false';
        }
    }
  
}

function startOver() {
    userClickedPattern = []
    gamePattern = []
    currentLevel = 0
    isStartOver = true
}






